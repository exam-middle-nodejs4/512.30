// Import thư viện express
const express = require('express');

// Tạo app
const app = new express();

// Import router
const { userRouter } = require('./App/Router/route');

// Sử dụng đc body json
app.use(express.json());

// Tạo cổng chạy
const port = 8000;

// Sử dụng router
app.use('/', userRouter);

app.listen(port, () => {
     console.log(`App listening to port ${port}`)});
